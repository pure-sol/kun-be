<?php

namespace App\Http\Controllers;

use App\Http\Resources\StudentResource;
use App\KunClass;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return StudentResource::collection(Student::paginate(25));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return new StudentResource($student);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'date_of_birth' => 'required|date',
                'class_id' => 'required|numeric|exists:classes,id',
            ]
        );

        if ($validation->fails()) {
            return response()->json(['error' => 'Input validation failed', 'data' => $validation->getMessageBag()->toArray()], 400);
        }

        // Prevent assiging student to an already full class, in production-grade software, this check would happen in a common place such as a respository or ClassService
        $class = KunClass::find($request->class_id);
        if ($class->students->count() >= $class->maximum_students) {
            return response()->json(['error' => 'Class is already full', 'data' => []]);
        }

        $class = Student::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'date_of_birth' => $request->date_of_birth,
            'class_id' => $request->class_id,
        ]);

        if (!$class) {
            return response()->json(['error' => 'Cound not create new student'], 403);
        }

        return new StudentResource($class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validation = Validator::make($request->all(),
            [
                'first_name' => 'string',
                'last_name' => 'string',
                'date_of_birth' => 'date',
                'class_id' => 'numeric|exists:classes,id',
            ]
        );

        if ($validation->fails()) {
            return response()->json(['error' => 'Input validation failed', 'data' => $validation->getMessageBag()->toArray()], 400);
        }

        // Prevent assiging student to an already full class, in production-grade software, this check would happen in a common place such as a respository or ClassService
        $class = KunClass::find($student->class_id);
        if ($class->students->count() >= $class->maximum_students) {
            return response()->json(['error' => 'Class is already full', 'data' => []]);
        }

        $data = $request->only(['first_name', 'last_name', 'date_of_birth', 'class_id']);

        foreach ($data as $field => $value) {
            $student->{$field} = $value;
        }

        $student->save();

        return new StudentResource($student);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return response()->json(null, 204);
    }
}
