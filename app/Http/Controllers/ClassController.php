<?php

namespace App\Http\Controllers;

use App\Http\Resources\ClassResource;
use App\Http\Resources\StudentResource;
use App\KunClass;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ClassResource::collection(KunClass::paginate(25));
    }

    public function show(KunClass $class){
        return new ClassResource($class);
    }

    public function getStudents(KunClass $class){
        return StudentResource::collection($class->students()->paginate(25));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'code' => 'required|unique:classes|alpha_dash',
                'name' => 'required',
                'maximum_students' => 'digits_between:0,1000',
                'status' => ['required', Rule::in(['open', 'closed'])],
                'description' => 'string|nullable',
            ]
        );

        if ($validation->fails()) {
            return response()->json(['error' => 'Input validation failed', 'data' => $validation->getMessageBag()->toArray()], 400);
        }

        $class = KunClass::create([
            'code' => $request->code,
            'name' => $request->name,
            'maximum_students' => isset($request->maximum_students) ? $request->maximum_students : 10,
            'status' => $request->status,
            'description' => $request->description,
        ]);

        if (!$class) {
            return response()->json(['error' => 'Cound not create new class'], 403);
        }

        return new ClassResource($class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Class  $class
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KunClass $class)
    {
        $validation = Validator::make($request->all(),
            [
                'code' => 'alpha_dash',
                'name' => 'string',
                'maximum_students' => 'digits_between:0,1000',
                'status' => [Rule::in(['open', 'closed'])],
                'description' => 'string|nullable',
            ]
        );

        if ($validation->fails()) {
            return response()->json(['error' => 'Input validation failed', 'data' => $validation->getMessageBag()->toArray()], 400);
        }

        $data = $request->only(['code', 'name', 'maximum_students', 'status', 'description']);

        foreach ($data as $field => $value) {
            $class->{$field} = $value;
        }

        $class->save();

        return new ClassResource($class);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Class  $class
     * @return \Illuminate\Http\Response
     */
    public function destroy(KunClass $class)
    {
        $class->delete();
        return response()->json(null, 204);
    }
}
